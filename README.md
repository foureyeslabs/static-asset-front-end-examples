# Static Asset Front-End Examples

This project holds a list of examples that are analyzed, within the experimental study of front-end methodologies
and design patterns. This particular set of examples, looks into the setup of leveraging a Content Distribution
Network, to deliver purely static content. Distribution, scalability, and robustness of leveraging this technique makes
it ideal for delivering high-performance always available front-ends.
